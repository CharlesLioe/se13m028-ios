//
//  ToDoItemr.h
//  TodoListApp
//
//  Created by Ming on 1/13/15.
//  Copyright (c) 2015 Ming. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ToDoItem : NSObject

@property NSString *itemName;
@property BOOL completed;
@property (readonly) NSDate *creationDate;

@end
