//
//  RepositoryListViewController.h
//  TodoListApp
//
//  Created by Ming on 1/13/15.
//  Copyright (c) 2015 Ming. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepositoryListViewController : UITableViewController
@property NSString *repositoryUrl;
@end
