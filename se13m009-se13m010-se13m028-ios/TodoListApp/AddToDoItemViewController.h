//
//  AddToDoItemViewController.h
//  TodoListApp
//
//  Created by Ming on 1/12/15.
//  Copyright (c) 2015 Ming. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToDoItem.h"

@interface AddToDoItemViewController : UIViewController

@property ToDoItem *toDoItem;

@end
