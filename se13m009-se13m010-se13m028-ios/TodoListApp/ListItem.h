//
//  ListItem.h
//  TodoListApp
//
//  Created by Ming on 1/13/15.
//  Copyright (c) 2015 Ming. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListItem : NSObject

@property NSString *itemName;
@property NSDictionary *jsonItem;

@end
