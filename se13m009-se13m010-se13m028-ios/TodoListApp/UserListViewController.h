//
//  UserListVIewControllerTableViewController.h
//  TodoListApp
//
//  Created by Ming on 1/13/15.
//  Copyright (c) 2015 Ming. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserListViewController : UITableViewController
@property NSString *userListUrl;
@property NSString *prompt;

@end
