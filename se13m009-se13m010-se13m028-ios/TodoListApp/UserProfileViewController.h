//
//  UserProfileViewController.h
//  TodoListApp
//
//  Created by Ming on 1/13/15.
//  Copyright (c) 2015 Ming. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListItem.h"

@interface UserProfileViewController : UIViewController
@property NSDictionary *UserItem;
@end
