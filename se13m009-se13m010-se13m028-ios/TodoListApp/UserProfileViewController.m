//
//  UserProfileViewController.m
//  TodoListApp
//
//  Created by Ming on 1/13/15.
//  Copyright (c) 2015 Ming. All rights reserved.
//

#import "UserProfileViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "UserListViewController.h"
#import "RepositoryListViewController.h"

@interface UserProfileViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *UserProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *UserNameLabel;

@end

@implementation UserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.UserNameLabel.text = self.UserItem[@"login"];
    NSString *avatarURLString = [self.UserItem valueForKey:@"avatar_url"];
    NSURL *avatarURL = [NSURL URLWithString:avatarURLString];
    if (avatarURL) {
        [self.UserProfileImageView setImageWithURL:avatarURL];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"BrowseUsers"]){
        UserListViewController *userBrowser = [segue destinationViewController];
        //nothing to do for now, let them select a new user from /users which is default behavior
    }
    if ([[segue identifier] isEqualToString:@"BrowseFollowers"]){
        UserListViewController *userBrowser = [segue destinationViewController];
        userBrowser.prompt = [@"select a Follower of " stringByAppendingString:(NSString*)self.UserItem[@"login"]];
        userBrowser.userListUrl = self.UserItem[@"followers_url"];
    }
    
    if ([[segue identifier] isEqualToString:@"BrowseRepository"]){
        RepositoryListViewController *repoBrowser = [segue destinationViewController];
        repoBrowser.repositoryUrl = self.UserItem[@"repos_url"];
    }
}


@end
